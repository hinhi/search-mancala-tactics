use std::collections::{HashMap, HashSet};

use crate::board::Board;

/// ゲームを進めるためのインターフェイス
pub trait Game {
    fn sow(&self, board: &mut Board, pos: usize);

    fn list_next(&self, board: &Board) -> HashSet<Board> {
        let mut set = HashSet::new();
        if board.is_finished() {
            return set;
        }
        let mut stack = vec![board.clone()];
        while let Some(board) = stack.pop() {
            for pos in board.iter_pos() {
                let mut copied = board.clone();
                self.sow(&mut copied, pos);
                if copied.side() == board.side() {
                    stack.push(copied);
                } else {
                    set.insert(copied);
                }
            }
        }
        set
    }

    fn list_next_with_pos(&self, board: &Board) -> HashMap<Board, Vec<usize>> {
        let mut map = HashMap::new();
        if board.is_finished() {
            return map;
        }
        let mut stack = vec![(board.clone(), vec![])];
        while let Some((board, pos_list)) = stack.pop() {
            for pos in board.iter_pos() {
                let mut copied = board.clone();
                let mut copied_pos = pos_list.clone();
                self.sow(&mut copied, pos);
                copied_pos.push(pos);
                if copied.side() == board.side() {
                    stack.push((copied, copied_pos));
                } else {
                    map.entry(copied).or_insert(copied_pos);
                }
            }
        }
        map
    }
}

/// ボードを評価するためのインターフェイス
pub trait BoardEvaluator {
    fn evaluate<G: Game>(&self, game: &G, board: &Board) -> i32;
}

/// 相手の種を盗るルールあり
#[derive(Debug, Clone)]
pub struct GameWithStealing;

impl GameWithStealing {
    pub fn new() -> GameWithStealing {
        GameWithStealing {}
    }
}

impl Game for GameWithStealing {
    fn sow(&self, board: &mut Board, pos: usize) {
        board.sow_with_stealing(pos);
    }
}

/// 相手の種は盗らないルール
#[derive(Debug, Clone)]
pub struct GameWithoutStealing;

impl GameWithoutStealing {
    pub fn new() -> GameWithoutStealing {
        GameWithoutStealing {}
    }
}

impl Game for GameWithoutStealing {
    fn sow(&self, board: &mut Board, pos: usize) {
        board.sow_without_stealing(pos);
    }
}

#[derive(Debug, Clone)]
pub struct DFSEvaluator {
    max_depth: usize,
}

impl DFSEvaluator {
    pub fn new(max_depth: usize) -> DFSEvaluator {
        DFSEvaluator { max_depth }
    }

    fn ab_search<G: Game>(game: &G, board: &Board, depth: usize, alpha: i32, beta: i32) -> i32 {
        if depth == 0 || board.is_finished() {
            return board.score_diff();
        }
        let mut alpha = alpha;
        for next in game.list_next(board) {
            let a = -Self::ab_search(game, &next, depth - 1, -beta, -alpha);
            if a > alpha {
                alpha = a;
            }
            if alpha >= beta {
                break;
            }
        }
        alpha
    }
}

impl BoardEvaluator for DFSEvaluator {
    fn evaluate<G: Game>(&self, game: &G, board: &Board) -> i32 {
        Self::ab_search(game, board, self.max_depth, -10000, 10000)
    }
}
