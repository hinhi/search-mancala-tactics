use search_mancala_tactics::prelude::*;

fn main() {
    let board = Board::new();
    let game = GameWithStealing::new();
    for depth in 0..15 {
        let evaluator = DFSEvaluator::new(depth);
        let score = evaluator.evaluate(&game, &board);
        println!("{} {}", depth, score);
    }
}
