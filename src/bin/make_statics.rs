use search_mancala_tactics::prelude::*;

fn list_boards<G: Game>(game: &G, depth: usize) -> Vec<Board> {
    let mut boards = vec![Board::new()];
    for _ in 0..depth {
        let mut next = Vec::new();
        for board in boards {
            for n in game.list_next(&board) {
                next.push(n);
            }
        }
        boards = next;
    }
    boards
}

fn main() {
    let game = GameWithoutStealing::new();
    let eval = DFSEvaluator::new(12);
    for board in list_boards(&game, 1) {
        println!("{}", board);
        let score = eval.evaluate(&game, &board);
        println!("score={}\n", score);
    }
}
