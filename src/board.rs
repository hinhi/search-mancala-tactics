use std::fmt::{self, Write};

pub const PIT: usize = 6;
pub const SEED: u8 = 4;

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct Board {
    side: usize,
    seeds: [[u8; PIT]; 2],
    score: [u8; 2],
}

impl Board {
    pub fn new() -> Board {
        Board {
            side: 0,
            seeds: [[SEED; PIT]; 2],
            score: [0, 0],
        }
    }

    pub fn side(&self) -> usize {
        self.side
    }

    pub fn seeds(&self) -> &[[u8; PIT]; 2] {
        &self.seeds
    }
    pub fn scores(&self) -> (u8, u8) {
        (self.score[0], self.score[1])
    }

    pub fn score_diff(&self) -> i32 {
        self.score[self.side] as i32 - self.score[1 - self.side] as i32
    }

    pub fn is_finished(&self) -> bool {
        self.seeds[0].iter().all(|s| *s == 0) || self.seeds[1].iter().all(|s| *s == 0)
    }

    fn move_seed(&mut self, side: usize, pos: usize, num: usize) -> (usize, usize) {
        if pos + num <= PIT {
            for i in pos..pos + num {
                self.seeds[side][i] += 1;
            }
            return (side, pos + num - 1);
        }
        for i in pos..PIT {
            self.seeds[side][i] += 1;
        }
        if self.side == side {
            self.score[side] += 1;
            if pos + num == PIT + 1 {
                return (side, PIT);
            }
            self.move_seed(1 - side, 0, pos + num - PIT - 1)
        } else {
            self.move_seed(1 - side, 0, pos + num - PIT)
        }
    }

    pub fn sow_with_stealing(&mut self, pos: usize) {
        let num = self.seeds[self.side as usize][pos];
        self.seeds[self.side][pos] = 0;
        let (end_side, end_pos) = self.move_seed(self.side, pos + 1, num as usize);
        if end_side == self.side {
            if end_pos == PIT {
                if !self.is_finished() {
                    return;
                }
            } else if self.seeds[self.side][end_pos] == 1 {
                let opposite_pos = PIT - 1 - end_pos;
                let opposite_num = self.seeds[1 - self.side][opposite_pos];
                self.seeds[self.side][end_pos] = 0;
                self.seeds[1 - self.side][opposite_pos] = 0;
                self.score[self.side] += opposite_num + 1;
            }
        }
        self.side = 1 - self.side;
    }

    pub fn sow_without_stealing(&mut self, pos: usize) {
        let num = self.seeds[self.side as usize][pos];
        self.seeds[self.side][pos] = 0;
        let (end_side, end_pos) = self.move_seed(self.side, pos + 1, num as usize);
        if end_side == self.side && end_pos == PIT && !self.is_finished() {
            return;
        }
        self.side = 1 - self.side;
    }

    pub fn iter_pos(&self) -> IterPos {
        IterPos {
            board: self,
            pos: 0,
        }
    }
}

pub struct IterPos<'a> {
    board: &'a Board,
    pos: usize,
}

impl<'a> Iterator for IterPos<'a> {
    type Item = usize;
    fn next(&mut self) -> Option<Self::Item> {
        for p in self.pos..PIT {
            if self.board.seeds[self.board.side][p] > 0 {
                self.pos = p + 1;
                return Some(p);
            }
        }
        self.pos = PIT;
        None
    }
}

impl fmt::Display for Board {
    fn fmt(&self, dest: &mut fmt::Formatter) -> fmt::Result {
        let mut s = String::new();
        if self.side == 1 {
            s += "* |";
        } else {
            s += "  |";
        }
        write!(s, "{:2}", self.score[1]).unwrap();
        write!(
            s,
            "|{}|  |",
            self.seeds[1]
                .iter()
                .rev()
                .map(|p| format!("{:2}", *p))
                .collect::<Vec<String>>()
                .join("|")
        )
        .unwrap();
        if self.side == 0 {
            s += "\n* |  ";
        } else {
            s += "\n  |  ";
        }
        write!(
            s,
            "|{}|",
            self.seeds[0]
                .iter()
                .map(|p| format!("{:2}", *p))
                .collect::<Vec<String>>()
                .join("|")
        )
        .unwrap();
        write!(s, "{:2}|", self.score[0]).unwrap();
        write!(dest, "{}", s)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn board64_with_stealing() {
        if PIT != 6 || SEED != 4 {
            return;
        }
        let mut board = Board::new();
        // 先手1
        board.sow_with_stealing(2);
        assert_eq!(0, board.side());
        assert_eq!((1, 0), board.scores());
        board.sow_with_stealing(5);
        assert_eq!(1, board.side());
        assert_eq!((2, 0), board.scores());
        // 後手1
        board.sow_with_stealing(5);
        assert_eq!(0, board.side());
        assert_eq!((2, 1), board.scores());
        // 先手2
        board.sow_with_stealing(0);
        assert_eq!(1, board.side());
        assert_eq!((8, 1), board.scores());
    }

    #[test]
    fn board64_without_stealing() {
        if PIT != 6 || SEED != 4 {
            return;
        }
        let mut board = Board::new();
        // 先手1
        board.sow_without_stealing(2);
        assert_eq!(board.side(), 0);
        assert_eq!(board.scores(), (1, 0));
        board.sow_without_stealing(5);
        assert_eq!(board.side(), 1);
        assert_eq!(board.scores(), (2, 0));
        // 後手1
        board.sow_without_stealing(5);
        assert_eq!(board.side(), 0);
        assert_eq!(board.scores(), (2, 1));
        // 先手2
        board.sow_without_stealing(0);
        assert_eq!(board.side(), 1);
        assert_eq!(board.scores(), (2, 1));
    }

    #[test]
    fn iter_pos() {
        let mut board = Board::new();

        let pos_list: Vec<_> = board.iter_pos().collect();
        let expect: Vec<_> = (0..PIT).collect();
        assert_eq!(pos_list, expect);

        board.sow_with_stealing(PIT - SEED as usize);
        let pos_list: Vec<_> = board.iter_pos().collect();
        let expect: Vec<_> = (0..PIT).filter(|p| *p != PIT - SEED as usize).collect();
        assert_eq!(pos_list, expect);
    }
}
