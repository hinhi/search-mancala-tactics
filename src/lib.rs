pub mod board;
pub mod game;

pub mod prelude {
    pub use crate::board::*;
    pub use crate::game::*;
}
